<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">

	<header id="masthead" class="site-header row" role="banner">
		<div class="container">
			<div class="col-md-12">

				<div class="site-branding">
				  <a href="<?php echo get_site_url(); ?>" class="custom-logo-link" rel="home"><img width="231" height="31" src="https://www.myaffordablehome.co.uk/wp-content/uploads/2020/06/logo.png" class="custom-logo" alt="Myaffordablehome"></a>
				</div>
				<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Primary Navigation">
				  <?php wp_nav_menu( array( 'menu' => 68 ) ); ?>
				</nav>

			</div>
		</div>
	</header><!-- #masthead -->

<?php if ( has_post_thumbnail()) : ?>
	<div class="ph-hero-image" style="background-image: url(<?php
	echo the_post_thumbnail_url(); ?>);">
	</div>
<?php endif; ?>
