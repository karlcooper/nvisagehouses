<?php
get_header();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		?>
		<div class="container">
			<div class="row">
				<?php
				if (is_front_page()) {
					echo '<div class="col-md-12">'.do_shortcode('[property_search_form id="default"]').'</div>';
				} else {
					echo '<h1 class="entry-title">'.get_the_title().'</h1>';
				}
				?>
				<div class="col-md-12">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php

		//
	} // end while
} // end if
get_footer(); ?>
