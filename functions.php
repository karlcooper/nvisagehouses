<?php


function mah_register_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'mah_register_menu' );

function mah_enqueue_styles()
{
  $ver = '1.1.2';
  wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.min.css' );
  wp_enqueue_style('standard-css', get_stylesheet_uri(), array(), $ver  );
  wp_enqueue_style('mah-css', get_stylesheet_directory_uri().'/css/custom.css', array(), $ver  );
  wp_enqueue_style( 'fawesome', "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" );
  wp_enqueue_script('mah-js', get_stylesheet_directory_uri().'/js/custom.js', array('jquery'), $ver  );
  wp_enqueue_script('mah-maps-places', 'https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDtMDAsp7b1tvqC2w9Jo8jNvereBmSmTPw', array(), $ver  );
  wp_deregister_script('googlemaps');
}
add_action('wp_enqueue_scripts', 'mah_enqueue_styles', 99);


$views = array(
    'list' => array(
        'default' => true,
        'content' => __( 'View In List', 'propertyhive' )
    ),
    'map' => array(
        'content' => __( 'View On Map', 'propertyhive' )
    )
);

function mah_add_grid_view($views) {
  if (is_array($views)) {
    $views['grid'] = array(
      'content' => __( 'View In Grid', 'propertyhive' )
    );
  }
  return $views;
}
add_filter( 'propertyhive_results_views', 'mah_add_grid_view' );

function mah_add_grid_view_body_class($classes) {
  if (isset($_GET['view'])) {
    if ($_GET['view'] == "grid") {
      array_push($classes,"ph-grid-view");
    }
  }
  return $classes;
}
add_filter( 'body_class', 'mah_add_grid_view_body_class' );

?>
