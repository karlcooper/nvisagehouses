<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $property, $actions;
?>

<?php
     if ( post_password_required() )
     {
        echo get_the_password_form();
        return;
     }
?>

<div id="property-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php

$gallery_attachments = $property->get_gallery_attachment_ids();

?>
<?php  ?>
	<div class="property-wrapper">
		<div class="gallery-slider">
    <?php
        if ( !empty($gallery_attachments) ) {
			$image = wp_get_attachment_image( $gallery_attachments[0], 'large' );
			echo '<div class="gallery-slider-main">' . $image . '</div>';

			echo '<div class="gallery-slider-pager"><div class="gallery-slider-pager-inner">';
            $i = 0;
			  $firsttime = true;
            foreach ($gallery_attachments as $gallery_attachment)
            {
                $image_title = esc_attr( get_the_title( $gallery_attachment ) );
                $image_link  = wp_get_attachment_url( $gallery_attachment );
                $image = wp_get_attachment_image( $gallery_attachment, 'large' );

				echo '<div class="property_inner ';
				if ($firsttime) echo ' active ';
				echo '">' . apply_filters( 'propertyhive_single_property_image_html', sprintf( '%s', $image ), $post->ID ) . '</div>';


                ++$i;
				$firsttime = false;

            }
			echo '</div>';

		  } else {

			echo apply_filters( 'propertyhive_single_property_image_html', sprintf( '<img src="%s" alt="Placeholder" />', ph_placeholder_img_src() ), $post->ID );

            }
       

      $actions = apply_filters( 'propertyhive_single_property_actions', $actions );
      

      do_action( 'propertyhive_property_actions_start' );
       ?>

			<script>
			jQuery( document ).ready( function($) {
			  $( '.gallery-slider .gallery-slider-pager-inner .property_inner' ).click( function() {
				$( this ).addClass( 'active' ).siblings().removeClass( 'active' );
				$( this ).closest( '.gallery-slider' ).find( '.gallery-slider-main' ).html( $( this ).html() );
			  });
			});
			</script>
			</div>
		</div><!-- end gallery-slider-->

		<div class="property-info-column">

			<div class="property-info-contact">
				<h4>Enquiries<br>Call: 020 234 567</h4>

        <?php
        $propertymeta = get_post_meta($property->id);

        if ($propertymeta['_reference_number'][0] != "") {
          echo '<span>Ref: '.$propertymeta['_reference_number'][0].'</span><br />';
        }
			?>

				<?php do_action( 'propertyhive_property_actions_end' ); ?>

			<!-- make an enquiry -->
			<a class="make-enquiry" data-fancybox data-src="#makeEnquiry<?php echo $post->ID; ?>" href="javascript:;"><?php _e( 'Make Enquiry', 'propertyhive' ); ?></a>

			<!-- LIGHTBOX FORM -->
			<div id="makeEnquiry<?php echo $post->ID; ?>" style="display:none;">
				
				<h2><?php _e( 'Make Enquiry', 'propertyhive' ); ?></h2>
				
				<p><?php _e( 'Please complete the form below and a member of staff will be in touch shortly.', 'propertyhive' ); ?></p>
				
				<?php propertyhive_enquiry_form(); ?>
        
				</div>
			<!-- END LIGHTBOX FORM -->
			<!-- end make an enquiry -->

			</div><!-- property-info-contact-->

			<div class="property-info-address">
<!-- 
<?php
            /**
             * propertyhive_single_property_summary hook
             * 
             * @hooked propertyhive_template_single_price - 10
             * 
             */
            do_action( 'propertyhive_single_property_summary' );
        ?> -->

<?php
					if ( $property->price != '' ) : ?>
					  <p class="property-info-price">  &#163;<?php echo $property->price; ?></p>
				<?php
					endif; ?>
				<!--<h1 >< ?php the_title(); ?></h1>-->
        <?php
        $propertyaddress = array();
        if ($propertymeta['_address_name_number'][0] != "") {
          array_push($propertyaddress,$propertymeta['_address_name_number'][0]);
        }
        if ($propertymeta['_address_street'][0] != "") {
          array_push($propertyaddress,$propertymeta['_address_street'][0]);
        }
        if ($propertymeta['_address_two'][0] != "") {
          array_push($propertyaddress,$propertymeta['_address_two'][0]);
        }
        if ($propertymeta['_address_three'][0] != "") {
          array_push($propertyaddress,$propertymeta['_address_three'][0]);
        }
        if ($propertymeta['_address_four'][0] != "") {
          array_push($propertyaddress,$propertymeta['_address_four'][0]);
        }
        if ($propertymeta['_address_postcode'][0] != "") {
          array_push($propertyaddress,$propertymeta['_address_postcode'][0]);
        }
        if ($propertymeta['_address_country'][0] != "") {
          array_push($propertyaddress,$propertymeta['_address_country'][0]);
        }
        ?>
       <p>
          <?php echo implode("<br>",$propertyaddress); ?>
        </p>

			</div><!-- property-info-address-->

			<div class="icon_area property-controls">
        
            <?php

            //---------to shortlist the properties----
            echo do_shortcode('[shortlist_button]');

            ?>
       
            </div>

			<div class="icon_area  social-sharing">
				
<?php echo do_shortcode("[addtoany]"); ?>


        </div>
		</div><!-- end property-info-colum --><div class="clearboth"></div>
	</div><!-- end property-wrapper -->

<!-- <div class="summary entry-summary">

<div class="property-info">

        < ?php
			// var_dump($property);
			//var_dump($property->_agent_id);
                ?>

		</div> 		<div class="clearboth"></div>
</div> .summary -->

    <?php

        $summary = get_the_excerpt();
    ?>

    <div class="property-further-info">
    <ul class="tabs">
        <li class="tab-link current" data-tab="tab-1">Overview</li>
        <li class="tab-link" data-tab="tab-2">Floorplans</li>
			<li class="tab-link" data-tab="tab-3">Features</li>
        <li class="tab-link" data-tab="tab-4">Fees</li>
			<li class="tab-link" data-tab="tab-5">Location</li>

      <?php
      $vton = false;
      $virtualtours = get_post_meta($property->id,'_virtual_tours',true);
      if (is_numeric($virtualtours)) {
        if ($virtualtours > 0) {
          $vton = true;
        }
      }

      if ($vton) {
        ?>
        <li class="tab-link" data-tab="tab-6">Video Tour</li>
        <?php
      }
      ?>
    </ul>
    <div class="property-further-info-content">
    <div id="tab-1" class="tab-content current">
    <div class="summary">
            <h4><?php //_e( 'Property Summary', 'propertyhive' ); ?></h4>

            <div class="property-further-details">

                <?php if ( $property->bedrooms > 0 ) { ?>
                    <i class="fas fa-bed"></i>
                    <span><?php echo $property->bedrooms; ?></span>
                <?php } ?>

                <?php if ( $property->bathrooms > 0 ) { ?>
                    <i class="fas fa-bath"></i>
                    <span><?php echo $property->bathrooms; ?></span>
                <?php } ?>
            </div>
            <p><?php echo nl2br($summary); ?></p>

        <?php

        $description = $property->get_formatted_description();

        if ( trim(strip_tags($description)) != '' )
        {
        ?>
        <div class="description">
            <h4><?php _e( 'Full Details', 'propertyhive' ); ?></h4>
            <?php echo $description; ?>

        </div>
        <?php }?>


    <ul>
       <?php

       do_action( 'propertyhive_property_meta_list_start' ); ?>
        <?php if ( $property->property_type != '' ) { ?><li class="type"><?php _e( 'Type', 'propertyhive' ); echo ': ' . $property->property_type; ?></li><?php } ?>

        <?php if ( $property->availability != '' ) { ?><li class="availability"><?php _e( 'Availability', 'propertyhive' ); echo ': ' . $property->availability; ?></li><?php } ?>
        <?php if ( $property->department != 'commercial' ) { ?>
        <?php if ( $property->reception_rooms > 0 ) { ?><li class="receptions"><?php _e( 'Reception Rooms', 'propertyhive' ); echo ': ' . $property->reception_rooms; ?></li><?php } ?>

        <?php if ( $property->parking != '' ) { ?><li class="parking"><?php _e( 'Parking', 'propertyhive' ); echo ': ' . $property->parking; ?></li><?php } ?>

        <?php if ( $property->outside_space != '' ) { ?><li class="outside-space"><?php _e( 'Outside Space', 'propertyhive' ); echo ': ' . $property->outside_space; ?></li><?php } ?>
        <?php
            switch ( $property->department )
            {
                case "residential-sales":
                {
        ?>
        <?php if ( $property->tenure != '' ) { ?><li class="tenure"><?php _e( 'Tenure', 'propertyhive' ); echo ': ' . $property->tenure; ?></li><?php } ?>

        <?php
                    break;
                }
                case "residential-lettings":
                {
        ?>

        <?php if ( $property->furnished != '' ) { ?><li class="furnished"><?php _e( 'Furnished', 'propertyhive' ); echo ': ' . $property->furnished; ?></li><?php } ?>
        <?php if ( $property->deposit > 0 ) { ?><li class="deposit"><?php _e( 'Deposit', 'propertyhive' ); echo ': ' . $property->get_formatted_deposit(); ?></li><?php } ?>
        <?php if ( $property->available_date != '') { ?><li class="available"><?php _e( 'Available', 'propertyhive' ); echo ': ' . $property->get_available_date(); ?></li><?php } ?>
        <?php
                    break;
                }
            }
        ?>

        <?php } // end if residential ?>

        <?php
       // do_action( 'propertyhive_property_meta_list_end' );
       // do_action( 'propertyhive_after_single_property_summary' );
        ?>
    </ul>

        </div>
    </div>
    <div id="tab-2" class="tab-content">
        <?php
        global $post, $property;

        $actions = array();

        $floorplan_ids = $property->get_floorplan_attachment_ids();

        if ( !empty( $floorplan_ids ) )
        {
            foreach ($floorplan_ids as $floorplan_id)
            {
                echo '<li><a href="'.wp_get_attachment_url( $floorplan_id ).'"><img src="'.wp_get_attachment_url( $floorplan_id ).'"/></a></li>';

            }

        }
        ?>
			</div>

    <div id="tab-3" class="tab-content">
        <?php $features = $property->get_features();

        if ( !empty($features) ){ ?>

            <div class="features">
                <h4><?php _e( 'Property Features', 'propertyhive' ); ?></h4>
                <ul>
                <?php
                foreach ($features as $feature)
                    { ?>
                    <li><p><?php echo $feature; ?></p></li>
                <?php
                    } ?>
                </ul>
					</div>
				<?php } ?>
			</div>

			<div id="tab-4" class="tab-content">
				<h4>Fees</h4>
				Waiting for data...
            </div>

			<div id="tab-5" class="tab-content">
        <div class="map-street-view-container">
          <div class="msv-buttons clearfix">
            <div class="msv-button msv-button-map active">
              Map view
            </div>
            <div class="msv-button msv-button-street">
              Street view
            </div>
          </div>
          <div class="msv-container msv-container-map active">
            <h4><?php _e( 'Map view', 'propertyhive' ); ?></h4>
            <?php echo do_shortcode('[property_map height="400"]')?>
          </div>
          <div class="msv-container msv-container-street">
            <h4><?php _e( 'Street view', 'propertyhive' ); ?></h4>
            <?php echo do_shortcode('[property_street_view height="400"]')?>
          </div>
    </div>
</div>

      <?php
      if ($vton) {
        ?>
        <div id="tab-6" class="tab-content">
            <p>Video Tour</p>
            <?php
            for ($x = 0; $x < $virtualtours; $x++) {
              $vt = get_post_meta($property->id,'_virtual_tour_'.$x,true);
              preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $vt, $matches);
              if (isset($matches[0])) {
                ?>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $matches[0]; ?>?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <?php
              }
            }
            ?>

        </div>
      <?php } ?>

			<!-- <?php  ?> -->
</div>
	</div><!-- property-further-info -->


</div><!-- #property-<?php the_ID(); ?> -->
