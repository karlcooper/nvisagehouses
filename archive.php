<?php
get_header();
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<article id="post-<?php echo get_the_ID(); ?>" class="post-<?php echo get_the_ID(); ?> post type-post status-publish format-standard has-post-thumbnail hentry category-news">
					  <header class="entry-header">
					    <span class="posted-on">Posted on <a href="https://www.myaffordablehome.co.uk/cras-pellentesque-vel-lacus-at-venenatis/" rel="bookmark"><time class="entry-date published updated"><?php the_time(); ?></time></a></span>
					    <h2 class="alpha entry-title"><a href="https://www.myaffordablehome.co.uk/cras-pellentesque-vel-lacus-at-venenatis/" rel="bookmark"><?php the_title(); ?></a></h2>
					  </header>
					  <!-- .entry-header -->
					  <aside class="entry-meta">
					    <div class="author">
								<?php
									echo get_avatar( get_the_author_meta( 'ID' ), 128 );
									echo '<div class="label">' . esc_attr( 'Written by' ) . '</div>';
									the_author_posts_link();
								?>
					    </div>
							<?php
							/* translators: used between list items, there is a space after the comma */
							$categories_list = get_the_category_list(  ', ' );

							if ( $categories_list ) : ?>
								<div class="cat-links">
									<?php
									echo '<div class="label">' . esc_attr(  'Posted in' ) . '</div>';
									echo wp_kses_post( $categories_list );
									?>
								</div>
							<?php endif; // End if categories. ?>

							<?php
							/* translators: used between list items, there is a space after the comma */
							$tags_list = get_the_tag_list( '',  ', ' );

							if ( $tags_list ) : ?>
								<div class="tags-links">
									<?php
									echo '<div class="label">' . esc_attr(  'Tagged' ) . '</div>';
									echo wp_kses_post( $tags_list );
									?>
								</div>
							<?php endif; // End if $tags_list. ?>
					  </aside>
					  <div class="entry-content">
					    <img width="506" height="342" src="https://www.myaffordablehome.co.uk/wp-content/uploads/2020/06/sample.jpg" class="attachment- size- wp-post-image" alt="" srcset="https://www.myaffordablehome.co.uk/wp-content/uploads/2020/06/sample.jpg 506w, https://www.myaffordablehome.co.uk/wp-content/uploads/2020/06/sample-300x203.jpg 300w" sizes="(max-width: 506px) 100vw, 506px">
					    <p>Donec sit amet tempor arcu. Curabitur ultrices risus ac nisl auctor, in gravida turpis cursus. Nulla sit amet nulla ornare, accumsan ipsum in, commodo ipsum. Duis rutrum facilisis mi faucibus aliquam. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
					    <p>Proin ac sem id ligula consequat eleifend. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut vitae urna dictum, venenatis mi vitae, euismod ex. Nam efficitur eros augue, quis porttitor felis fringilla in. In eu lacinia quam. Nulla eleifend risus a euismod vulputate.</p>
					  </div>
					  <!-- .entry-content -->
					</article>
				</div>
			</div>
		</div>
		<?php

		//
	} // end while
} // end if
get_footer(); ?>
