jQuery( document ).ready( function($) {
  function init_ph_navs() {
    if ((!$( '.gallery-slider .gallery-slider-main .yhh-gallery-nav-left' ).length) && ($( '.gallery-slider .gallery-slider-pager .property_inner' ).length > 1)) {
      $( '.gallery-slider .gallery-slider-main' ).append( '<div class="yhh-gallery-nav-left"><i class="fa fa-chevron-left"></i></div><div class="yhh-gallery-nav-right"><i class="fa fa-chevron-right"></i></div>' );
    }
    $( '.gallery-slider .gallery-slider-main .yhh-gallery-nav-left' ).click( function() {
      var pager = $( this ).closest( '.gallery-slider' ).find( '.gallery-slider-pager-inner' );
      if ($( pager ).find( '.property_inner.active' ).prev().length) {
        $( pager ).find( '.property_inner.active' ).prev().trigger( 'click' );
      } else {
        $( pager ).find( '.property_inner:last-child' ).trigger( 'click' );
      }
    });

    $( '.gallery-slider .gallery-slider-main .yhh-gallery-nav-right' ).click( function() {
      var pager = $( this ).closest( '.gallery-slider' ).find( '.gallery-slider-pager-inner' );
      if ($( pager ).find( '.property_inner.active' ).next().length) {
        $( pager ).find( '.property_inner.active' ).next().trigger( 'click' );
      } else {
        $( pager ).find( '.property_inner:first-child' ).trigger( 'click' );
      }
    });
  }
  init_ph_navs();
  $('.gallery-slider .gallery-slider-main').bind('DOMSubtreeModified', function(){
    init_ph_navs();
  });

  $( '.property-further-info .tab-link' ).click( function() {
    $( '.property-further-info .tab-link' ).removeClass( 'current' );
    $( this ).addClass( 'current' );

    $( '.tab-content' ).hide();
    $( '.tab-content#'+$( this ).attr('data-tab') ).show();
  });

  function homepage_department_selectors() {
    $( 'form.property-search-form .control-department label input' ).each( function() {
      if ($( this ).is(':checked')) {
        $( this ).parent().addClass( 'selected' );
      } else {
        $( this ).parent().removeClass( 'selected' );
      }
    });
  }

  homepage_department_selectors();
  $( 'form.property-search-form .control-department label input' ).change( function() {
    homepage_department_selectors();
  });
});

function initialize_autocomplete() {
  var input = document.getElementById('address_keyword');
  var options = {
    types: ['geocode'],
    componentRestrictions: {country: 'gb'}
  };
  if (input!=null){
    var autocomplete = new google.maps.places.Autocomplete(input, options);
 }
}
if(window.addEventListener) {
  window.addEventListener('load', initialize_autocomplete);
}else{
  window.attachEvent('onload', initialize_autocomplete);
}
