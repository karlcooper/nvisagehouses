<div class="clearfix"></div>

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="row">
  		<div class="col-md-12">
    		<?php echo wp_nav_menu( array( 'menu' => 69 ) ); ?>
      </div>
  		<div class="col-md-12">
      	© My Affordable Home 2020. All rights reserved. Company No. 1234567. VAT No. 1234567
        <br>
        <a href="https://www.nvisage.co.uk" target="_blank" rel="noopener noreferrer">Web design</a> &amp; <a href="https://www.nvisage.co.uk" target="_blank" rel="noopener noreferrer">web development</a> by Nvisage LTD
      </div>
    </div>
  </div>
  <!-- .col-full -->
</footer>


<?php wp_footer(); ?>

</body>
</html>
